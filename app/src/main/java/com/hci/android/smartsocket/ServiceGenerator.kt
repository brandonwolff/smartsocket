package com.hci.android.smartsocket

import android.content.Context
import android.util.Log
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

class ServiceGenerator {
    companion object {
        const val API_BASE_URL = "https://graph.api.smartthings.com"
        const val API_OAUTH_REDIRECT = "com.hci.android.smartsocket://oauth"
        private const val PREFS_NAME = "switchWidgetPrefs"

        private var gson = GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create()

        private lateinit  var httpClient: OkHttpClient.Builder

        private lateinit var builder: Retrofit.Builder

        private lateinit var retrofit: Retrofit

        private lateinit var mToken: AccessToken

        fun <S> createService(serviceClass: Class<S>): S {
            httpClient = OkHttpClient.Builder()
            builder = Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))

            val client = httpClient.build()
            retrofit = builder.client(client).build()
            return retrofit.create(serviceClass)
        }

        fun <S> createService(serviceClass: Class<S>, accessToken: AccessToken?, c: Context): S {
            httpClient = OkHttpClient.Builder()
            builder = Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))

            if (accessToken != null) {
                mToken = accessToken
                httpClient.addInterceptor { chain ->
                    val original = chain.request()

                    val requestBuilder = original.newBuilder()
                            .header("Accept", "application/json")
                            .header("Content-Type", "application/json")
                            .header("Authorization", "${mToken.tokenType} ${mToken.accessToken}")
                            .method(original.method(), original.body())

                    val request = requestBuilder.build()
                    chain.proceed(request)
                }

                httpClient.authenticator(object : Authenticator{
                    override fun authenticate(route: Route?, response: Response?): Request? {
                        if (responseCount(response) >= 2) {
                            // If both the original call and the call with refreshed token failed,
                            // it will probably keep failing, so don't try again.
                            return null
                        }

                        // We need a new client, since we don't want to make another call using our client with access token
                        val tokenService = createService(LoginService::class.java)
                        val call = tokenService.getRefreshAccessToken(mToken.refreshToken,
                                mToken.clientId, mToken.clientSecret, API_OAUTH_REDIRECT,
                                "refresh_token")
                        try {
                            val tokenResponse = call.execute()
                            val prefs = c.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
                            if (tokenResponse.code() == 200) {
                                val newToken = tokenResponse.body()
                                mToken = newToken!!
                                mToken.clientId = prefs.getString("oauth.clientId", null)
                                mToken.clientSecret = prefs.getString("oauth.clientSecret", null)
                                prefs.edit().putBoolean("oauth.loggedin", true).apply()
                                prefs.edit().putString("oauth.accesstoken", newToken.accessToken).apply()
                                prefs.edit().putString("oauth.refreshtoken", newToken.refreshToken).apply()
                                prefs.edit().putString("oauth.tokentype", newToken.tokenType).apply()
                                Log.d("ServiceGenerator", "Token: ${newToken.accessToken} Refresh: ${newToken.refreshToken} Type: ${newToken.tokenType}")

                                return response?.request()?.newBuilder()
                                        ?.header("Authorization", "${newToken.tokenType} ${newToken.accessToken}")
                                        ?.build()
                            } else {
                                prefs.edit().putBoolean("oauth.loggedin", false).apply()
                                return null
                            }
                        } catch (e: IOException) {
                            e.printStackTrace()
                            return null
                        }
                    }
                })

            }

            val client = httpClient.build()
            retrofit = builder.client(client).build()
            return retrofit.create(serviceClass)
        }

        private fun responseCount(response: Response?): Int {
            var result = 1
            var loopResponse = response?.priorResponse()
            while(loopResponse != null) {
                result++
                loopResponse = loopResponse.priorResponse()
            }
            return result
        }
    }
}