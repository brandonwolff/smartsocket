package com.hci.android.smartsocket

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.EditText

/**
 * The configuration screen for the [SwitchWidget] AppWidget.
 */
class SwitchWidgetConfigureActivity : AppCompatActivity() {
    private var mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID
    private lateinit var mDaysText: EditText
    private lateinit var mHoursText: EditText
    private lateinit var mMinutesText: EditText
    private var mOnClickListener: View.OnClickListener = View.OnClickListener {
        val context = this@SwitchWidgetConfigureActivity

        // When the button is clicked, store the string locally
        val days = mDaysText.text.toString()
        val hours = mHoursText.text.toString()
        val minutes = mMinutesText.text.toString()
        val timerPref = (days.toInt() * 86400000) + (hours.toInt() * 3600000) + (minutes.toInt() * 60000)
        saveTimerPref(context, timerPref)

        // It is the responsibility of the configuration activity to update the app widget
        val appWidgetManager = AppWidgetManager.getInstance(context)
        SwitchWidget.updateAppWidget(context, appWidgetManager, mAppWidgetId)

        val manager: AlarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val i = Intent(context, SwitchWidget::class.java)
        i.action = Globals.ACTION_UPDATE_TIME

        val timerIntent = PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_CANCEL_CURRENT)
        manager.cancel(timerIntent)

        // Make sure we pass back the original appWidgetId
        val resultValue = Intent()
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId)
        setResult(Activity.RESULT_OK, resultValue)
        finish()
    }

    public override fun onCreate(icicle: Bundle?) {
        super.onCreate(icicle)

        // Set the result to CANCELED.  This will cause the widget host to cancel
        // out of the widget placement if the user presses the back button.
        setResult(Activity.RESULT_CANCELED)

        setContentView(R.layout.switch_widget_configure)
        mDaysText = findViewById(R.id.daysText)
        mHoursText = findViewById(R.id.hoursText)
        mMinutesText = findViewById(R.id.minutesText)
        findViewById<Button>(R.id.add_button).setOnClickListener(mOnClickListener)

        // Find the widget id from the intent.
        val extras = intent.extras
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID)
        }

        // If this activity was started with an intent without an app widget ID, finish with an error.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish()
            return
        }

        val time = loadTimerPref(this@SwitchWidgetConfigureActivity)
        mDaysText.setText(String.format("%02d",time/86400000))
        mHoursText.setText(String.format("%02d",(time%86400000)/3600000))
        mMinutesText.setText(String.format("%02d",(time%3600000)/60000))
    }

    companion object {
        // Write the prefix to the SharedPreferences object for this widget
        internal fun saveTimerPref(context: Context, time: Int) {
            val prefs = context.getSharedPreferences(Globals.PREF_FILE_KEY, 0).edit()
            prefs.putInt("timerLength", time)
            prefs.apply()
        }

        // Read the prefix from the SharedPreferences object for this widget.
        // If there is no preference saved, get the default from a resource
        internal fun loadTimerPref(context: Context): Int {
            val prefs = context.getSharedPreferences(Globals.PREF_FILE_KEY, 0)
            return prefs.getInt("timerLength", 14400000)
        }

        internal fun deleteTimerPref(context: Context) {
            val prefs = context.getSharedPreferences(Globals.PREF_FILE_KEY, 0).edit()
            prefs.remove("timerLength")
            prefs.apply()
        }
    }
}

