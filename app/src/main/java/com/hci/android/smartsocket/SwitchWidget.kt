package com.hci.android.smartsocket

import android.app.AlarmManager
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.SystemClock
import android.util.Log
import android.util.TypedValue
import android.widget.RemoteViews
import android.widget.Switch
import kotlin.concurrent.timer


/**
 * Implementation of App Widget functionality.
 * App Widget Configuration implemented in [SwitchWidgetConfigureActivity]
 */
class SwitchWidget : AppWidgetProvider() {
    private var timerIntent: PendingIntent? = null

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)

        val prefs = context.getSharedPreferences(Globals.PREF_FILE_KEY, Context.MODE_PRIVATE)
        val endpoint = prefs.getString("api.endpoint", null)
        val token = AccessToken()
        token.accessToken = prefs.getString("oauth.accesstoken", null)
        token.refreshToken = prefs.getString("oauth.refreshtoken", null)
        token.tokenType = prefs.getString("oauth.tokentype", null)
        token.clientId = prefs.getString("oauth.clientId", null)
        token.clientSecret = prefs.getString("oauth.clientSecret", null)
        val client = ServiceGenerator.createService(OutletService::class.java, token, context)

        val views = RemoteViews(context.packageName, R.layout.switch_widget)

        val newIntent = Intent(context, SwitchWidget::class.java)

        if (intent.action == Globals.ACTION_UPDATE_TIME) {
            val totalTime = prefs.getInt("timerLength", 14400000).toLong()
            val startTime = prefs.getLong("timerStart", 0)
            if (startTime > 0) {
                val timeLeft = totalTime - (SystemClock.elapsedRealtime() - startTime)
                views.setTextViewText(R.id.timerText, String.format("%02d:%02d:%02d", timeLeft / 86400000, (timeLeft % 86400000) / 3600000, (timeLeft % 3600000) / 60000))
                if (timeLeft <= 0) {
                    newIntent.action = Globals.ACTION_SWITCH_ON
                    context.sendBroadcast(newIntent)
                }
            } else {
                views.setTextViewText(R.id.timerText, "00:00:00")
            }
        } else if (intent.action == Globals.ACTION_SWITCH_ON) {
            val thread = Thread(Runnable {
                val onCall = client.sendCommand("$endpoint/switches/on")
                if (onCall.execute().isSuccessful)
                    prefs.edit().putBoolean("switch_on", true).apply()
                else
                    Log.e("SwitchWidget", "Failed to send on command.")
            })
            thread.start()
            thread.join()

            if (prefs.getBoolean("switch_on", false)) {

                newIntent.action = Globals.ACTION_SWITCH_OFF
                val pendingIntent = PendingIntent.getBroadcast(context, 0, newIntent, PendingIntent.FLAG_UPDATE_CURRENT)
                views.setOnClickPendingIntent(R.id.switchButton, pendingIntent)

                val manager: AlarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
                val i = Intent(context, SwitchWidget::class.java)
                i.action = Globals.ACTION_UPDATE_TIME

                if (timerIntent == null) {
                    timerIntent = PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_CANCEL_CURRENT)
                }
                manager.cancel(timerIntent)

                views.setImageViewResource(R.id.switchButton, R.drawable.lightbulb_on)
                views.setTextViewText(R.id.timerText, "00:00:00")
            }
        } else if (intent.action == Globals.ACTION_SWITCH_OFF) {

            val thread = Thread(Runnable {
                val offCall = client.sendCommand("$endpoint/switches/off")
                if (offCall.execute().isSuccessful)
                    prefs.edit().putBoolean("switch_on", false).apply()
                else
                    Log.e("SwitchWidget", "Failed to send off command.")
            })
            thread.start()
            thread.join()

            if (!prefs.getBoolean("switch_on", false)) {
                views.setImageViewResource(R.id.switchButton, R.drawable.lightbulb_off)
                newIntent.action = Globals.ACTION_SWITCH_ON
                val pendingIntent = PendingIntent.getBroadcast(context, 0, newIntent, PendingIntent.FLAG_UPDATE_CURRENT)
                views.setOnClickPendingIntent(R.id.switchButton, pendingIntent)

                val startTime = SystemClock.elapsedRealtime()
                val manager: AlarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
                val i = Intent(context, SwitchWidget::class.java)
                i.action = Globals.ACTION_UPDATE_TIME

                if (timerIntent == null) {
                    timerIntent = PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_CANCEL_CURRENT)
                }
                manager.setRepeating(AlarmManager.ELAPSED_REALTIME, startTime, 60000, timerIntent)
                prefs.edit().putLong("timerStart", startTime).apply()
                val totalTime = prefs.getInt("timerLength", 14400000)
                views.setTextViewText(R.id.timerText, String.format("%02d:%02d:%02d", totalTime / 86400000, (totalTime % 86400000) / 3600000, (totalTime % 3600000) / 60000))
            }
        }

        val appWidget = ComponentName(context, SwitchWidget::class.java)
        val appWidgetManager = AppWidgetManager.getInstance(context)
        appWidgetManager.updateAppWidget(appWidget, views)
    }

    companion object {
        internal fun updateAppWidget(context: Context, appWidgetManager: AppWidgetManager,
                                     appWidgetId: Int) {
            val prefs = context.getSharedPreferences(Globals.PREF_FILE_KEY, Context.MODE_PRIVATE)
            val textSize = prefs.getString("timerTextSize", "40").toFloat()
            // Construct the RemoteViews object
            val views = RemoteViews(context.packageName, R.layout.switch_widget)
            // Construct an Intent which is pointing to this class.
            val intent = Intent(context, SwitchWidget::class.java)
            if (prefs.getBoolean("switch_on", false)) {
                intent.action = Globals.ACTION_SWITCH_OFF
                views.setImageViewResource(R.id.switchButton, R.drawable.lightbulb_on)
                val pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
                views.setOnClickPendingIntent(R.id.switchButton, pendingIntent)
            }
            else {
                intent.action = Globals.ACTION_SWITCH_ON
                views.setImageViewResource(R.id.switchButton, R.drawable.lightbulb_off)
                val pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
                views.setOnClickPendingIntent(R.id.switchButton, pendingIntent)
            }
            views.setTextViewTextSize(R.id.timerText, TypedValue.COMPLEX_UNIT_SP, textSize)

            val timerColor = prefs.getString("timerColor", "#53BCEB")
            views.setInt(R.id.widgetContainer, "setBackgroundColor", Color.parseColor(timerColor))

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
    }
}

