package com.hci.android.smartsocket

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    companion object {
        const val TAG = "LoginActivity"
    }

    private lateinit var prefs: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor
    private val textWatcher = object : TextWatcher {
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            loginButton.isEnabled = !(client_secret.text.isNullOrEmpty() || client_id.text.isNullOrEmpty())
            settingsButton.isEnabled = !(client_secret.text.isNullOrEmpty() || client_id.text.isNullOrEmpty())
        }

        override fun afterTextChanged(p0: Editable?) {
            // Auto-generated stub
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            // Auto-generated stub
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        prefs = getSharedPreferences(Globals.PREF_FILE_KEY, Context.MODE_PRIVATE)
        editor = prefs.edit()

        client_id.addTextChangedListener(textWatcher)
        client_secret.addTextChangedListener(textWatcher)

        client_id.setText(prefs.getString("oauth.clientId", "ac3bed62-327d-4084-ac53-e6aceb43bb0e"))
        client_secret.setText(prefs.getString("oauth.clientSecret", "900f265f-9ddd-4184-9807-70a28c7d51e0"))

        loginButton.setOnClickListener {
            val clientId = client_id.text.toString()
            val clientSecret = client_secret.text.toString()
            editor.putString("oauth.clientId", clientId)
            editor.putString("oauth.clientSecret", clientSecret)
            editor.apply()
            val intent = Intent(Intent.ACTION_VIEW,
                    Uri.parse("${Globals.API_LOGIN_URL}&client_id=$clientId&scope=app" +
                            "&redirect_uri=${Globals.API_OAUTH_REDIRECT}"))
            // This flag is set to prevent the browser with the login form from showing in the history stack
            intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY

            startActivity(intent)
            finish()
        }
    }

    override fun onResume() {
        super.onResume()

        // The intent filter defined in AndroidManifest will handle the return from ACTION_VIEW intent
        val uri = intent.data ?: null
        if (uri != null && uri.toString().startsWith(Globals.API_OAUTH_REDIRECT)) {
            val code = uri.getQueryParameter("code")
            if (code != null) {
                // Get access token
                val thread = Thread(Runnable {
                    val clientId = prefs.getString("oauth.clientId", null)
                    val clientSecret = prefs.getString("oauth.clientSecret", null)
                    val loginService = ServiceGenerator.createService(LoginService::class.java)
                    val tokenCall = loginService.getAccessToken(code, clientId, clientSecret, Globals.API_OAUTH_REDIRECT, "authorization_code")
                    val response = tokenCall.execute()
                    if (response.code() == 200) {
                        val token = response.body()
                        editor.putBoolean("oauth.loggedin", true)
                        editor.putString("oauth.accesstoken", token?.accessToken)
                        editor.putString("oauth.refreshtoken", token?.refreshToken)
                        editor.putString("oauth.tokentype", token?.tokenType)
                        editor.apply()
                    } else {
                        editor.putBoolean("oauth.loggedin", false).apply()
                        Log.e(TAG, "OAuth response failed!")
                    }
                })

                thread.start()
                thread.join()
                if (prefs.getBoolean("oauth.loggedin", false))
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
            } else if (uri.getQueryParameter("error") != null) {
                Log.e(TAG, uri.getQueryParameter("error"))
            }

        }


    }

    fun settings(v: View) {
        startActivity(Intent(this, MainActivity::class.java))
    }

}
