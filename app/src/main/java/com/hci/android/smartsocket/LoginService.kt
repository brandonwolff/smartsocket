package com.hci.android.smartsocket

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.*

interface LoginService {
    @FormUrlEncoded
    @POST("/oauth/token")
    fun getAccessToken(
            @Field("code") code: String?,
            @Field("client_id") clientId: String?,
            @Field("client_secret") clientSecret: String?,
            @Field("redirect_uri") redirectUri: String,
            @Field("grant_type") grantType: String): Call<AccessToken>

    @FormUrlEncoded
    @POST("/oauth/token")
    fun getRefreshAccessToken(
            @Field("refresh_token") refreshToken: String?,
            @Field("client_id") clientId: String?,
            @Field("client_secret") clientSecret: String?,
            @Field("redirect_uri") redirectUri: String,
            @Field("grant_type") grantType: String): Call<AccessToken>

    @GET("/api/smartapps/endpoints")
    fun getEndpoint(@Query("access_token") accessToken: String): Call<List<JsonObject>>
}