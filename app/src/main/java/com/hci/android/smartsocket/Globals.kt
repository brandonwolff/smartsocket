package com.hci.android.smartsocket

object Globals {
    const val ACTION_SWITCH_ON = "ACTION_SWITCH_ON"
    const val ACTION_SWITCH_OFF = "ACTION_SWITCH_OFF"
    const val ACTION_UPDATE_TIME = "ACTION_UPDATE_TIME"
    const val ACTION_START_TIMER = "ACTION_START_TIMER"
    const val ACTION_STOP_TIMER = "ACTION_STOP_TIMER"
    const val PREF_FILE_KEY = "switchWidgetPrefs"
    const val API_LOGIN_URL = "https://graph.api.smartthings.com/oauth/authorize?response_type=code"
    const val API_OAUTH_REDIRECT = "com.hci.android.smartsocket://oauth"
}