package com.hci.android.smartsocket

class AccessToken {

    var accessToken: String? = null
    // OAuth requires uppercase Authorization HTTP header value for token type
    var tokenType: String? = null
        get() {
            if (field.isNullOrEmpty()) {
                return null
            }

            if (!Character.isUpperCase(field!![0])) {
                tokenType = Character.toString(field!![0]).toUpperCase() + field!!.substring(1)
            }

            return field
        }
    var expiresIn: Int? = null
    var refreshToken: String? = null
    var scope: String? = null
    var clientId: String? = null
    var clientSecret: String? = null
}
