package com.hci.android.smartsocket

import android.app.Activity
import android.appwidget.AppWidgetManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.*
import com.google.gson.JsonObject
import com.pes.androidmaterialcolorpickerdialog.ColorPicker
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    private var mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID
    private lateinit var mDaysText: EditText
    private lateinit var mHoursText: EditText
    private lateinit var mMinutesText: EditText
    private lateinit var mToggleButton: ToggleButton
    private lateinit var mTextSize: Spinner
    private lateinit var mColorButton: Button
    private lateinit var mOutletName: TextView
    private lateinit var mToken: AccessToken
    private lateinit var prefs: SharedPreferences

    private var mOnClickListener: View.OnClickListener = View.OnClickListener {
        Toast.makeText(this, R.string.saving, Toast.LENGTH_SHORT).show()

        val context = this

        // When the button is clicked, store the string locally
        val days = mDaysText.text.toString()
        val hours = mHoursText.text.toString()
        val minutes = mMinutesText.text.toString()
        val timerPref = (days.toInt() * 86400000) + (hours.toInt() * 3600000) + (minutes.toInt() * 60000)
        saveTimerPref(timerPref)

        Toast.makeText(this, R.string.saved_message, Toast.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mDaysText = mainDaysText
        mHoursText = mainHoursText
        mMinutesText = mainMinutesText
        mTextSize = textSize
        mToggleButton = toggleButton
        mColorButton = findViewById(R.id.colorButton)

        prefs = getSharedPreferences(Globals.PREF_FILE_KEY, Context.MODE_PRIVATE)
        mToken = AccessToken()
        mToken.accessToken = prefs.getString("oauth.accesstoken", null)
        mToken.refreshToken = prefs.getString("oauth.refreshtoken", null)
        mToken.tokenType = prefs.getString("oauth.tokentype", null)
        mToken.clientId = prefs.getString("oauth.clientId", null)
        mToken.clientSecret = prefs.getString("oauth.clientSecret", null)

        if (!prefs.getBoolean("oauth.loggedin", false)) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        } else {
            getEndpoint()
        }


        save_button.setOnClickListener(mOnClickListener)
        val time = loadTimerPref()
        mDaysText.setText(String.format("%02d",time/86400000))
        mHoursText.setText(String.format("%02d",(time%86400000)/3600000))
        mMinutesText.setText(String.format("%02d",(time%3600000)/60000))

        val spinnerArray = IntArray(32) { (2*it)+10 }
        val arrayAdapter = ArrayAdapter<Int>(this, android.R.layout.simple_spinner_item, spinnerArray.toTypedArray())
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        mTextSize.adapter = arrayAdapter

        mTextSize.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
                val item = parent.getItemAtPosition(pos)
                prefs.edit().putString("timerTextSize", item.toString()).apply()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                prefs.edit().putString("timerTextSize", "40").apply()
            }
        }

        mTextSize.setSelection(spinnerArray.indexOf(prefs.getString("timerTextSize", "40").toInt()))

        changeTimerColor()

        mToggleButton.setOnCheckedChangeListener { compoundButton, checked ->
            val endpoint = prefs.getString("api.endpoint", null)
            val outletService = ServiceGenerator.createService(OutletService::class.java, mToken, this)
            if (checked) {
                val onCall = outletService.sendCommand("$endpoint/switches/on")
                onCall.enqueue(object: Callback<Void> {
                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                        if (response.isSuccessful) {
                            prefs.edit().putBoolean("switch_on", true).commit()
                        } else {
                            Log.d("MainActivity", "On command failed!")
                        }
                    }

                    override fun onFailure(call: Call<Void>?, t: Throwable?) {
                        Log.e("MainActivity", "Failed to send on command.")
                    }
                })
            } else {
                val offCall = outletService.sendCommand("$endpoint/switches/off")
                offCall.enqueue(object: Callback<Void> {
                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                        if (response.isSuccessful) {
                            prefs.edit().putBoolean("switch_on", false).commit()
                        } else {
                            Log.d("MainActivity", "Off command failed!")
                        }
                    }

                    override fun onFailure(call: Call<Void>?, t: Throwable?) {
                        Log.e("MainActivity", "Failed to send off command.")
                    }
                })
            }
        }

        checkOutlets()
    }

    override fun onResume() {
        super.onResume()
        checkOutlets()
        changeTimerColor()
    }

    private fun saveTimerPref(time: Int) {
        prefs.edit().putInt("timerLength", time).apply()
    }

    private fun loadTimerPref(): Int {
        return prefs.getInt("timerLength", 14400000)
    }

    fun login(v: View) {
        startActivity(Intent(this, LoginActivity::class.java))
    }

    private fun checkOutlets() {
        val endpoint = prefs.getString("api.endpoint", null)
        if (!endpoint.isNullOrEmpty()) {
            val token = AccessToken()
            token.accessToken = prefs.getString("oauth.accesstoken", null)
            token.refreshToken = prefs.getString("oauth.refreshtoken", null)
            token.tokenType = prefs.getString("oauth.tokentype", null)
            token.clientId = prefs.getString("oauth.clientId", null)
            token.clientSecret = prefs.getString("oauth.clientSecret", null)
            val outletService = ServiceGenerator.createService(OutletService::class.java, token, this)
            val outletCall = outletService.getOutlets("$endpoint/switches")
            outletCall.enqueue(object : Callback<List<JsonObject>> {
                override fun onResponse(call: Call<List<JsonObject>>, response: Response<List<JsonObject>>) {
                    if (response.isSuccessful) {
                        val outletList = response.body()
                        findViewById<TextView>(R.id.outletName).text = outletList?.get(0)?.get("name")?.asString
                        findViewById<ToggleButton>(R.id.toggleButton).isChecked = outletList?.get(0)?.get("value")?.asString == "on"
                    } else {
                        Log.d("MainActivity", "Outlet check failed!")
                    }
                }

                override fun onFailure(call: Call<List<JsonObject>>?, t: Throwable?) {
                    Log.d("MainActivity", "Outlet check request failed!")
                }
            })
        } else {
            android.os.Handler().postDelayed({ checkOutlets() }, 100)
        }
    }

    private fun getEndpoint() {
        if (prefs.getBoolean("oauth.loggedin", false)) {
            val loginService = ServiceGenerator.createService(LoginService::class.java, mToken, this)
            val endpointCall = loginService.getEndpoint(prefs.getString("oauth.accesstoken", null))
            endpointCall.enqueue(object: Callback<List<JsonObject>> {
                override fun onResponse(call: Call<List<JsonObject>>, response: Response<List<JsonObject>>) {
                    val statusCode = response.code()
                    if (statusCode == 200) {
                        val clientList = response.body()
                        prefs.edit().putString("api.endpoint", clientList?.get(0)?.get("uri")?.asString).commit()
                    } else {
                        Log.d("MainActivity", "API Endpoint response failed!")
                    }
                }

                override fun onFailure(call: Call<List<JsonObject>>?, t: Throwable?) {
                    Log.e("MainActivity", "API Endpoint request failed!")
                }
            })
        }
    }

    fun openColorPicker(v: View) {
        val cp = ColorPicker(this, 0x53, 0xBC, 0xEB)
        cp.show()
        cp.enableAutoClose()
        cp.setCallback { color ->
            val colorString = String.format("#%06X", 0xFFFFFF and color)
            prefs.edit().putString("timerColor", colorString).commit()
            changeTimerColor()
        }
    }

    private fun changeTimerColor() {
        val timerColor = prefs.getString("timerColor", "#53bceb")
        mColorButton.text = timerColor
        mColorButton.setTextColor(Color.parseColor(timerColor))
    }
}
