package com.hci.android.smartsocket

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Url

interface OutletService {
    @GET
    fun getOutlets(@Url url: String): Call<List<JsonObject>>

    @PUT
    fun sendCommand(@Url url: String): Call<Void>
}